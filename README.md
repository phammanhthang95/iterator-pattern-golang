<p>Nếu chưa tối ưu bằng iterator pattern thì đoạn code sẽ như sau<p>
package main

import "fmt"

type user struct {
name string
age int
}

type userCollection struct {
users []*user
}

func (u *userCollection) getUsers() []*user {
return u.users
}

func main() {
user1 := &user{
name: "a",
age: 15,
}
user2 := &user{
name: "b",
age: 16,
}
userCollection := &userCollection{
users: []*user{user1, user2},
}
users := userCollection.getUsers()
for i := 0; i < len(users); i++ {
fmt.Printf("User is %+v\n", users[i])
}
}